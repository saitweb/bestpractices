Best Practices
==============

A collection of lessons learned (and documented) by the SAIT Web Programming team.


General
-------
* 

Design Patterns
---------------
* Facade
* State
* Singleton
* Simple Factory

Object-Oriented Design
----------------------

* Use the facade pattern to simplify controllers
* Extract value objects when parameters can be confusing
* Classes should not be longer than 100 lines
* Methods should not be longer than 5 lines
* Methods should only do one thing
* Fewer parameters are better
* Use Getters and Setters


Testing
-------

* Write the test for the best use case of the code
* There's no need to test private methods
* Make sure tests don't send emails... they are slow
* Avoid database interaction, but if you do, make sure you clean up BEFORE and AFTER the tests run


##Links

###AngularJS
* [Data Binding with Services](http://stsc3000.github.io/blog/2013/10/26/a-tale-of-frankenstein-and-binding-to-service-values-in-angular-dot-js/ "Data Binding with Services")
* [Promises and .then() .catch().  The right way to do it.] (https://github.com/petkaantonov/bluebird/wiki/Promise-anti-patterns#the-thensuccess-fail-anti-pattern)