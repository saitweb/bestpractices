angular.module('ngApp', [])

.service('service', function ($http, $timeout) {
    var t = this;
    this.data={
      "somedata":42,
      "delay" :500
    };

    function addThenAdd(){
      $timeout(function() {
        t.data.somedata = parseInt(t.data.somedata) + 1;
        addThenAdd();
      }, t.data.delay);
    };


    addThenAdd();
})
 
.controller('ctrl', function ($scope, service) {
    $scope.service = service;
    $scope.thatData = service.data.somedata;
    $scope.$watch('service.data.somedata', function (newValue) {
        $scope.value = newValue;
    });
    
});