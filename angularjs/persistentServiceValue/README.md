#Persistent Service Values

Angular is particular in how it will persist service values bound to the scope.

Binding values directly to the scope will simply copy them, fortunately you can bind data objects or the service itself.
