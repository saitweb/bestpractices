'use strict';

angular.module('saitCsoWidgetApp')
  .controller('MainCtrl', function ($scope, jobFactory, settings) {

    $scope.jobs = jobFactory.getJobs();

  });
