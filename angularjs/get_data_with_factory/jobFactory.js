'use strict';

angular.module('saitCsoWidgetApp')
  .factory('jobFactory', function ($http) {

    var jobs = {rows:null}
    var api = 'http://staging.umt.edu/api/v1/jsonify/xml/https://rss.myinterfase.com/rss/montana_All_Active_Jobs_RSS_xml.xml'

    var getJobs = function () {
        if(jobs.rows === null){
            $http.get(api).success(function(result){
                jobs.rows = result.data.Row
            })
        }
        return jobs
    }

    return {
        getJobs: getJobs
    }

  });
