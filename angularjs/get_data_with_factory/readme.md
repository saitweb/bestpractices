Getting data with a Factory
===========================

Overview
--------

The controller calls a Factory, that returns a Singleton object that has methods to get data. That object then resolves the promises for you so that controllers are simplified.

This example works as long as you don't need to handle the promise outside of the Factory.

Benefits
--------

Using a Factory (or a Service) allows you to centralize the code that gets data, rather than fetching it in the controller directly.

Yeoman
------

Run `yo angular:factory jobFactory`

You may need to rename the file to the proper case in some versions of Yeoman