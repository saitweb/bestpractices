- [Tab Demo](http://accessibility.athena-ict.com/aria/examples/tabpanel2.shtml)

- [Live Regions](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Live_Regions)

- [ARIA Atomic](http://msdn.microsoft.com/en-us/library/windows/apps/hh968001.aspx)

Accessible Angular Tabs
-----------------------

tabs.js overwrites [angular-ui bootstrap](http://angular-ui.github.io/bootstrap/) v0.8.0  tab directive templates to make them use aria tags.

tabs.html is the template for using directive in an accessible manner

