angular.module('template/tabs/tab.html', []).run(['$templateCache', function($templateCache) {
  $templateCache.put('template/tabs/tab.html',
    '<li  ng-class="{active: active, disabled: disabled}">\n' +
      '  <a href="javascript:void(0);" ng-click="select()" role="tab" aria-selected="{{active?\'true\':\'false\'}}" tab-heading-transclude>{{heading}}</a>\n' +
      '</li>\n' +
      '');
}]);

angular.module("template/tabs/tabset-titles.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("template/tabs/tabset-titles.html",
    "<ul id=\"tablist\" role=\"tablist\" class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical}\">\n" +
      "</ul>\n" +
      "");
}]);

angular.module("template/tabs/tabset.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("template/tabs/tabset.html",
    "\n" +
      "<div class=\"tabbable\">\n" +
      "  <ul class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical}\" ng-transclude>\n" +
      "  </ul>\n" +
      "  <div aria-live=\"polite\"  class=\"tab-content\">\n" +
      "    <div class=\"tab-pane\" \n" +
      "         ng-repeat=\"tab in tabs\" \n" +
      "         ng-class=\"{active: tab.active}\"\n" +
      "         tab-content-transclude=\"tab\"\n" +
      "         id=\"panel{{$index}}\"\n" +
      "         aria-labeledby=\"tab{{$index}}\"\n" +
      "         role=\"tabpanel\">\n"+
      "    </div>\n" +
      "  </div>\n" +
      "</div>\n" +
      "");
}]);