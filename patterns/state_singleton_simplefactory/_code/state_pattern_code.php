<?php


// class Student{

// 	const NOT_APPLIED = 0;
// 	const APPLIED = 1;
// 	const ACCEPTED = 2;

// 	private $state;

// 	public function __construct(){
// 		$this->state = NOT_APPLIED;
// 	}

// 	public function apply(){
// 		if($this->state === NOT_APPLIED){
// 			$this->state = APPLIED;
// 		}
// 		else if($this->state === APPLIED){
// 			throw new Exception('The student has yet to apply');
// 		}
// 		else if($this->state === ACCEPTED){
// 			throw new Exception('The student has already been accepted');
// 		}
// 	}

// 	public function accept(){
// 		if($this->state === NOT_APPLIED){
// 			throw new Exception('The student has yet to apply');
// 		}
// 		else if($this->state === APPLIED){
// 			$this->state = ACCEPTED;
// 		}
// 		else if($this->state === ACCEPTED){
// 			throw new Exception('The student has already been accepted');
// 		}
// 	}

// }




class Student{
	private $not_applied_state;
	private $applied_state;
	private $accepted_state;

	private $state;

	public function __construct(){
		$this->not_applied_state = new NotAppliedState($this);
		$this->applied_state = new AppliedState($this);
		$this->accepted_state = new AcceptedState($this);

		$this->state = $this->not_applied_state;
	}

	public function accept(){
		$this->state->accept();
	}

	public function apply(){
		$this->state->apply();	
	}

	public function set_state($state){
		$this->state = $state;
	}

	public function get_not_applied_state(){
		return $this->not_applied_state;
	}

	public function get_applied_state(){
		return $this->applied_state;
	}

	public function get_accepted_state(){
		return $this->accepted_state;
	}
}

interface StudentApplyState{
	public function apply();
	public function accept();
}

class NotAppliedState implements StudentApplyState{

	public $student;

	public function __construct($student){
		$this->student = $student;
	}

	public function apply(){
		$this->student->set_state($student->get_applied_state());
	}

	public function accept(){
		throw new Exception('The student has yet to apply');
	}

}

class AppliedState implements StudentApplyState{

	public $student;

	public function __construct($student){
		$this->student = $student;
	}

	public function apply(){
		throw new Exception('The student has already applied');
	}

	public function accept(){
		$this->student->set_state($student->get_accepted_state());
	}

}

class AcceptedState implements StudentApplyState{

	public $student;

	public function __construct($student){
		$this->student = $student;
	}

	public function apply(){
		throw new Exception('The student has already been accepted');
	}

	public function accept(){
		throw new Exception('The student has already been accepted');
	}

}