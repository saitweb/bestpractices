[Facade Pattern](http://c2.com/cgi/wiki?FacadePattern)
==============


Intent 
------
Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use. This can be used to simplify a number of complicated object interactions into a single interface.

Good uses
---------

* Limiting the ammount of logic in a controller
