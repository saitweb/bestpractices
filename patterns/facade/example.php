<?php


// controllers/ticketapi.php
class TicketApi{
	public static function getTickets(){
		$ticketDFacade = new TicketDashboardFacade($this->user);
		$this->api->out($ticketDFacade);
	}
}



// libraries/facades/ticketdashboardfacade.php

class TicketDashboardFacade{
	public $filter = null;
	
	public function __construct($user){
		$this->filter = new TicketFilter($user);
	}
	
	public function ticketList(){
		return Tickets::getByFilter($this->filter);
	}
}

// libraries/ticketfilter.php

class TicketFilter{
	private $current_tags = array();
	private $departments = array();
	private $search_term = '';
	private $user = null;
	
	public function __construct($user){
		$this->user = $user;
	}
	
	public function get_current_search_terms(){
		// get things from the session and cookie and set them
		return '';
	}
	
	public function set_current_search_terms(){
			
	}
}